// NOTE https://github.com/emilk/egui
// NOTE https://docs.rs/egui/latest/egui/widgets/plot/struct.Plot.html
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

// use std::f64::consts::PI;
// use eframe::{egui, emath::Numeric};
// use egui::plot::BarChart;
use eframe::egui;
use egui::plot::{Line, Plot, PlotPoints};

fn main() {
    // Check the first arument isn't help
    let args: Vec<String> = std::env::args().collect();
    if args.len() == 1 || args[1] == "help" || args[1] == "--help" || args[1] == "-h" {
        println!("Usage: egui-tensorboard <file1.dat> <file2.dat>");
        std::process::exit(0);
    }
    // Check there are 3 arguments
    if args.len() != 3 {
        println!("Usage: egui-tensorboard <file1.dat> <file2.dat>");
        std::process::exit(0);
    }

    let options = eframe::NativeOptions::default();
    eframe::run_native(
        "My egui App",
        options,
        Box::new(|_cc| Box::new(MyApp::default())),
    );
}

struct MyApp {
    start: f64,
    end: f64,
    n: i32, // Moving Average
    log: bool,
}

impl Default for MyApp {
    fn default() -> Self {
        Self {
            start: 0.0,
            end: 10.0,
            n: 1,
            log: false,
        }
    }
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::SidePanel::left("Left Panel").show(ctx, |ui| {
            // a scrollable area
            egui::ScrollArea::vertical().show(ui, |ui| {
                // A label showing log scale status
                ui.label(format!("Log Scale: {}", self.log));
                // A checkbox to toggle log scale
                ui.checkbox(&mut self.log, "Log Scale");
            });
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            egui::ScrollArea::vertical().show(ui, |ui| {
                // Get the first and second command line arguments
                // TODO allow anynumber of dat files
                let args: Vec<String> = std::env::args().collect();
                let filename = &args[1];
                let filename2 = &args[2];

                // Read the files
                let y1 = read_dat_file(filename.to_string());
                let y2 = read_dat_file(filename2.to_string());

                // Get the Moving Average
                let mut y1 = moving_average(y1, self.n);
                let mut y2 = moving_average(y2, self.n);
                if self.log {
                    y1 = y1.iter().map(|x| x.ln()).collect();
                    y2 = y2.iter().map(|x| x.ln()).collect();
                }
                ui.heading("Errors");

                // Add a slider to scale the moving average wich is self.n
                ui.add(
                    egui::Slider::new(&mut self.n, 1..=((y1.len() as i32) / 10) as i32)
                        .text("Moving Average"),
                );

                let x1: Vec<[f64; 2]> = (1..y1.len())
                    .zip(y1.iter())
                    .map(|(a, b)| {
                        let a = a as f64;
                        [a.clone(), b.clone()]
                    })
                    .collect();
                let x2: Vec<[f64; 2]> = (1..y2.len())
                    .zip(y2.iter())
                    .map(|(a, b)| {
                        let a = a as f64;
                        [a.clone(), b.clone()]
                    })
                    .collect();
                // Cast into points
                let p1: PlotPoints = x1.into();
                let p2: PlotPoints = x2.into();
                // Make a line
                let line1 = Line::new(p1);
                let line2 = Line::new(p2);
                // Plot the values
                Plot::new("Both Errors")
                    .view_aspect(2.0)
                    .show(ui, |plot_ui| {
                        plot_ui.line(line1);
                        plot_ui.line(line2);
                    });
            });
        });
    }
}

fn read_dat_file(file: String) -> Vec<f64> {
    let row = std::fs::read_to_string(file)
        .expect("Something went wrong reading the file")
        .lines()
        // This next method would be good for multiple columns
        // .map(|line| line.split_whitespace().map(|s| s.parse().unwrap()).collect::<Vec<f64>>())
        .map(|line| line.trim().parse::<f64>().unwrap())
        .collect::<Vec<f64>>();

    return row;
}

fn moving_average(y: Vec<f64>, n: i32) -> Vec<f64> {
    // If the moving average isn't specified, do nothing
    if n < 2 {
        return y
    }
    // Calculate the moving average
    let mut yavg: Vec<f64> = vec![0.0; y.len()];
    for i in 0..y.len() {
        let mut sum = 0.0;
        for j in 0..n {
            if i as i32 - j >= 0 {
                sum += y[i - j as usize];
            }
        }
        yavg[i] = sum / (n as f64);
    }

    // Drop the first n values
    for _ in 0..n {
        yavg.remove(0);
    }

    return yavg;
}
