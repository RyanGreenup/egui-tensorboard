// NOTE https://github.com/emilk/egui
// NOTE https://docs.rs/egui/latest/egui/widgets/plot/struct.Plot.html
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

use std::f64::consts::PI;

use eframe::{egui, emath::Numeric};
use egui::plot::BarChart;
use egui::plot::{Bar, Line, Plot, PlotPoints};
use hdf5;
use ndarray;
use ndarray::Array3;
use ndarray::ArrayBase;
use ndarray::Dim;
use ndarray::OwnedRepr;
use num::Complex;
use rustfft;

fn main() {
    // read_dat_file(std::env::args().nth(2).unwrap().to_string());

    let options = eframe::NativeOptions::default();
    eframe::run_native(
        "My egui App",
        options,
        Box::new(|_cc| Box::new(MyApp::default())),
    );
}

struct MyApp {
    freqs: Vec<f64>,
    t: f64,
    nsignals: usize,
    tvals: Vec<f64>,
    superpos: Vec<f64>,
    N: usize,
}

impl Default for MyApp {
    fn default() -> Self {
        Self {
            N: 10000,
            nsignals: 1,
            freqs: vec![1.0],
            t: 1.0,
            tvals: vec![0.0],
            superpos: vec![0.0],
        }
    }
}

fn fft(x: &Vec<[f64; 2]>, fs: f64, t: f64) -> Vec<[f64; 2]> {
    // Unpack the array
    let x = x.clone().to_owned();
    let mut y: Vec<f64> = x.iter().map(|v| v[1]).collect();
    let x: Vec<f64> = x.iter().map(|v| v[0]).collect();

    let N: usize = x.len();

    // Perform a forward FFT of size 1234
    use rustfft::{num_complex::Complex, FftPlanner};

    let mut planner = FftPlanner::<f64>::new();
    let fft = planner.plan_fft_forward(N);

    let mut buffer = vec![Complex { re: 0.0, im: 0.0 }; N];
    for i in 0..N {
        buffer[i] = Complex { re: y[i], im: 0.0 }
    }
    fft.process(&mut buffer);

    // Processing the FFt....................................................
    // Return the modulus of FFT div by N
    let buffer: Vec<f64> = buffer
        .iter()
        .map(|v| v.norm_sqr().sqrt() / (N as f64))
        .collect();
    let xvals: Vec<f64> = (1..N).map(|i| (i as f64) * t / (N as f64)).collect();

    // Split it in half
    let (buffer, _) = buffer.split_at((N / 2) as usize);
    let (xvals, _) = xvals.split_at((N / 2) as usize);

    let out: Vec<[f64; 2]> = (1..(N / 2)).map(|i| [xvals[i], buffer[i]]).collect();

    return out;
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::SidePanel::left("Left Panel").show(ctx, |ui| {
            // a scrollable area
            egui::ScrollArea::vertical().show(ui, |ui| {
                // Zero the superposition
                self.superpos = vec![0.0; self.tvals.len()];

                for i in 1..self.nsignals {
                    // Choose the frequency
                    ui.add(
                        egui::Slider::new(&mut self.freqs[i], 0.0..=10.0).text("2π * Frequency"),
                    );

                    // Get the function
                    let f: Vec<[f64; 2]> = self
                        .tvals
                        .iter()
                        .map(|v| {
                            let y = (2.0 * PI * self.freqs[i] * v).sin();
                            [v.clone(), y]
                        })
                        .collect();

                    // Get the superposition
                    self.superpos = self
                        .tvals
                        .iter()
                        .zip(self.superpos.iter())
                        .map(|(a, b)| a + b)
                        .collect();

                    // plot the points
                    let p: PlotPoints = f.into();
                    let line = Line::new(p);
                    // Plot the values
                    Plot::new(format!("Signal {}", i))
                        .view_aspect(2.0)
                        .show(ui, |plot_ui| plot_ui.line(line));
                }
            });
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            egui::ScrollArea::vertical().show(ui, |ui| {
                ui.heading("Signal Plotting");

                if ui.add(egui::Button::new("Add Signal")).clicked() {
                    self.nsignals += 1;
                    self.freqs.push(1.0);
                }

                // Adjust the time scale length
                ui.add(egui::Slider::new(&mut self.t, 0.0..=(30.0)).text("Moving Average"));
                self.tvals = (1..self.N as usize)
                    .map(|i| {
                        // rescale to length of t
                        let t = (i as f64) * (self.t) / (self.N as f64);
                        t
                    })
                    .collect::<Vec<f64>>();

                // Get the first and second command line arguments
                let args: Vec<String> = std::env::args().collect();
                let filename = &args[1];
                let filename2 = &args[2];


                let y1 = read_dat_file(filename.to_string());
                let y1 = moving_average(y1, self.t as i32);
                let y2 = read_dat_file(filename2.to_string());
                let y2 = moving_average(y2, self.t as i32);


                let x1: Vec<[f64; 2]> = self.tvals.iter().zip(y1.iter()).map(|(a, b)| [a.clone(), b.clone()]).collect();
                let x2: Vec<[f64; 2]> = self.tvals.iter().zip(y2.iter()).map(|(a, b)| [a.clone(), b.clone()]).collect();
                // Cast into points
                let p1: PlotPoints = x1.into();
                let p2: PlotPoints = x2.into();
                // Make a line
                let line1 = Line::new(p1);
                let line2 = Line::new(p2);
                // Plot the values
                Plot::new("Both Errors")
                    .view_aspect(2.0)
                    .show(ui, |plot_ui| {
                        plot_ui.line(line1);
                        plot_ui.line(line2);
                    });

            });
        });
    }
}

fn read_dat_file(file: String) -> Vec<f64> {
    let row = std::fs::read_to_string(file)
        .expect("Something went wrong reading the file")
        .lines()
        // This next method would be good for multiple columns
        // .map(|line| line.split_whitespace().map(|s| s.parse().unwrap()).collect::<Vec<f64>>())
        .map(|line| line.trim().parse::<f64>().unwrap())
        .collect::<Vec<f64>>();

    return row;
}

fn moving_average(y: Vec<f64>, n: i32) -> Vec<f64> {
    // Calculate the moving average
    let mut yavg: Vec<f64> = vec![0.0; y.len()];
    for i in 0..y.len() {
        let mut sum = 0.0;
        for j in 0..n {
            if i as i32 - j >= 0 {
                sum += y[i - j as usize];
            }
        }
        yavg[i] = sum / (n as f64);
    }

    // Drop the first n values
    for _ in 0..n {
        yavg.remove(0);
    }

    return yavg;
}
