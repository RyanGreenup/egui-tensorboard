# eGUI Tensorboard

This is a previewer that will draw a plot of two signals stored in `.dat` files.

![](./preview.png)

The `.dat` files should look like this:


```
~/Models/
└── training.dat
        0.8
        0.7
        0.6
        ...
        ...
└── testing.dat
        0.81
        0.5
        0.6
        ...
        ...
```

## Getting started

  1. Download the source code
  2. `cargo install --path .`
  3. `~/.cargo/bin/egui-tensorboard /path/to/testing.dat /path/to/training.dat`



## Further Info

### Writing losses to a file

I use something like this to write losses to a file:


<details>

```python
class lossWriter():

    def __init__(self, run_name: str | None):
        self.losses = {"train": [], "test": []}
        if run_name is not None:
            self.name: str = run_name
        else:
            self.name: str = str(int(time.time()))

        ## create a directory for the run
        dir = os.path.join(os.getcwd(), "runs", f"{self.name}")
        os.makedirs(dir, exist_ok=True)

        self.training_file = open(os.path.join(dir, "training_losses.dat"), "w")
        self.testing_file = open(os.path.join(dir,  "testing_losses.dat"), "w")


        ## Automatically close the file when the class is deconstructed
        def __del__(self):
            self.training_file.close()
            self.testing_file.close()


    def log_loss(self, testing_loss: float, training_loss: float, write: bool = True):
        """
        Log the loss of the network

        Args:
            testing_loss: The loss of the testing data
            training_loss: The loss of the training data
            write: Whether to write the loss to a file (just cals self.write_loss as a convenience)

        """
        self.losses["train"].append(training_loss)
        self.losses["test"].append(testing_loss)
        if write:
            self.write_loss(testing_loss, training_loss)

    def write_loss(self, testing_loss: float, training_loss: float):
        """
        Write the loss to the file

        Args:
            testing_loss: The loss of the testing data
            training_loss: The loss of the training data

        """
        self.training_file.write(f"{training_loss}\n")
        self.testing_file.write(f"{testing_loss}\n")
        ## Flush the files
        self.training_file.flush()
        self.testing_file.flush()


    def log_hyperparams(self,
                        net: FF_NN_02,
                        lr: float,
                        epochs: int,
                        batch_size: int,
                        opt: str,
                        loss_func: str,
                        testing_error: float,
                        training_error: float,
                        write: bool = False):

        self.neurons_per_layer = {name: module.weight.shape[0] for name, module in net.named_modules() if isinstance(module, nn.Linear)}
        self.hyperparams = {"testing_error": testing_error, "training_error": training_error, "lr": lr, "epochs": epochs, "batch_size": batch_size, "opt": opt, "loss_func": loss_func, "neurons_per_layer": self.neurons_per_layer }
        if write:
            self.write_hyperparams()


    def write_hyperparams(self):
        dir = os.path.join(os.getcwd(), "runs", self.name)
        file = os.path.join(dir, "hparams.json")
        with open(file, "w") as f:
            json.dump(self.hyperparams, f)

    def plot_loss_file(self, testing_loss_file: str, training_loss_file: str):
        ## Load both files from disk and plot them both on the same graph
        with open(testing_loss_file, "r") as f:
            testing_loss = f.read().splitlines()
            ## Change to list of floats
            testing_loss = [float(x) for x in testing_loss]
        with open(training_loss_file, "r") as f:
            training_loss = f.read().splitlines()
            ## Change to list of floats
            training_loss = [float(x) for x in training_loss]

        plt.plot(training_loss, label="Training Loss")
        plt.plot(testing_loss, label="Testing Loss")
        plt.legend()
        plt.show()

    def plot_loss(self):
        losses = self.losses
        plt.plot(losses["train"], label="Training Loss")
        plt.plot(losses["test"], label="Testing Loss")
        plt.legend()
        plt.show()
```


</details>
