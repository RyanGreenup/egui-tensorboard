.PHONY: help

help:
	@echo "Automatically create symlinks"
	@echo "Please use 'make <target>' where <target> is:"
	@echo "----------"
	@echo " "
	@echo " mount     Mount the target and debug"
	@echo " unmount   Remove the ramdisk storing the data"

# https://www.jamescoyle.net/how-to/943-create-a-ram-disk-in-linux
mount:
	cargo clean
	doas mount -t tmpfs -o size=4G tmpfs "debug"  --mkdir
	doas mount -t tmpfs -o size=4G tmpfs "target" --mkdir

unmount:
	doas umount "target"
	doas umount "debug"
	doas rm -r target debug

