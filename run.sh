#!/bin/sh
TRAIN="training.dat"
TEST="testing.dat"

main() {
    # Create the training and testing files
    touch "${TRAIN}" "${TEST}"

    # Run the program in the background
    cargo run --release  -- "${TEST}"  "${TRAIN}" &

    # Let the model train
    append_random_numbers_to_file

    # Clean up
    rm "${TRAIN}" "${TEST}"
}

random_num() {
    seq 1 100 | shuf | head -n 1
}

append_random_numbers_to_file() {
    while true; do
        random_num >> "${TRAIN}"
        random_num >> "${TEST}"
        sleep 0.5
    done
}

main
